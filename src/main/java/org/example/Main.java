package org.example;

import org.example.enums.ExoEnum;
import org.example.exo.*;
import org.example.heritage.*;
import org.example.poo.Compte;
import org.example.poo.Epargne;
import org.example.poo.Simple;
import org.example.poo.Payant;

public class Main {
    public static void main(String[] args) {
        //Exo.getComptageDeMot();
        //Exo.getComptageOccurrence();
        //Exo.getAnagramme();
        //Exo.getPalindrome();
        //Exo.getPyramide();

        //Switch.getSwitchEvenement();

        //Structure.getTabExo1();
        //Structure.getDecalage();

        //Tableau.getTailleTab();

        //ExoEnum.getPriority();

        //JustePrix.trouverLePrix();

//        Compte compte0 = new Compte(1500);
//        compte0.versement(400.45);
//        System.out.println(compte0.toString());
//        System.out.println("");
//        Simple compte1 = new Simple(1000,200);
//        compte1.verifDecouvert(100);
//        System.out.println("");
//        Epargne compte2 = new Epargne(1000,5.5);
//        compte2.calculInteret();
//        System.out.println("");
//        Payant compte3 = new Payant(1500);
//        compte3.versementPayant(400);

        Person person1 = new Person(50);
        person1.hello();

        Student student1 = new Student(15);
        student1.hello();
        student1.goToClasses();
        student1.displayAge();

        Teacher teacher1 = new Teacher(40);
        teacher1.hello();
        teacher1.explain("pourquoi un subject en paramètre ?");

//        Personne personne = new Personne("Thomas",new Appartement(new Door("blue")));
//        personne.display();
    }
}