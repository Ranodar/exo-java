package org.example.pendu;

public class LePendu {

    public int nbEssai;
    public String masque;
    public String motATrouver;

    public LePendu() {
    }

    public void testChar(){

    }

    public void testWin(){

    }

    public void genererMasque(){

    }

    public int getNbEssai() {
        return nbEssai;
    }

    public void setNbEssai(int nbEssai) {
        this.nbEssai = nbEssai;
    }

    public String getMasque() {
        return masque;
    }

    public void setMasque(String masque) {
        this.masque = masque;
    }

    public String getMotATrouver() {
        return motATrouver;
    }

    public void setMotATrouver(String motATrouver) {
        this.motATrouver = motATrouver;
    }

    @Override
    public String toString() {
        return "LePendu{" +
                "nbEssai= " + nbEssai +
                ", masque= '" + masque + '\'' +
                ", motATrouver= '" + motATrouver + '\'' +
                '}';
    }
}
