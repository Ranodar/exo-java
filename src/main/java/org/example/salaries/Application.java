package org.example.salaries;

public class Application {

    public static void main(String[] args){

        Salarie john = new Salarie("abcdef","lead","management","john",5000);
        Salarie bob = new Salarie("qdfesf","back","management","bob",4000);
        Salarie mohamed = new Salarie("frgvtr","front","management","mohamed",1500);
        Salarie.nbSalaries();

        john.afficherSalaire();
        bob.afficherSalaire();
        mohamed.afficherSalaire();

        Salarie.totalSalaireMensuel();

        Salarie.resetCount();
        Salarie.nbSalaries();


    }
}
