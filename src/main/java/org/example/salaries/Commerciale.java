package org.example.salaries;

public class Commerciale extends Salarie{

    public int ca;
    public int commission;

    public Commerciale(String matricule, String categorie, String service, String nom, int salaire, int ca, int commission) {
        super(matricule, categorie, service, nom, salaire);
        this.ca = ca;
        this.commission = commission;
    }

    public int getCa() {
        return ca;
    }

    public void setCa(int ca) {
        this.ca = ca;
    }

    public int getCommission() {
        return commission;
    }

    public void setCommission(int commission) {
        this.commission = commission;
    }

    @Override
    public String toString() {
        return "Commerciale{" +
                "ca=" + ca +
                ", commission=" + commission +
                ", id=" + id +
                ", matricule='" + matricule + '\'' +
                ", categorie='" + categorie + '\'' +
                ", service='" + service + '\'' +
                ", nom='" + nom + '\'' +
                ", salaire=" + salaire +
                '}';
    }
}
