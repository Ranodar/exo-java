package org.example.salaries;

public class Salarie {

    public int id = 0;
    public static int count;

    public static int totalSalaireMensuel;
    public String matricule;
    public String categorie;
    public String service;
    public String nom;
    public int salaire;

    public Salarie(String matricule, String categorie, String service, String nom, int salaire) {
        this.id = ++count;
        this.matricule = matricule;
        this.categorie = categorie;
        this.service = service;
        this.nom = nom;
        this.salaire = salaire;
        totalSalaireMensuel += this.salaire;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Salarie.count = count;
    }

    public static int getTotalSalaireMensuel() {
        return totalSalaireMensuel;
    }

    public static void setTotalSalaireMensuel(int totalSalaireMensuel) {
        Salarie.totalSalaireMensuel = totalSalaireMensuel;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getSalaire() {
        return salaire;
    }

    public void setSalaire(int salaire) {
        this.salaire = salaire;
    }

    public void afficherSalaire(){
        System.out.println("Le salaire de "+nom+" est de "+salaire+" euros");
    }

    public static void nbSalaries(){
        System.out.println("Le nombre de salariés est de "+count);
    }

    public static void totalSalaireMensuel(){
        System.out.println("Le montant total des salaires mensuel est de "+totalSalaireMensuel);
    }

    public static void resetCount(){
        count = 0;
    }

    @Override
    public String toString() {
        return "Salarie{" +
                "id=" + id +
                ", matricule='" + matricule + '\'' +
                ", categorie='" + categorie + '\'' +
                ", service='" + service + '\'' +
                ", nom='" + nom + '\'' +
                ", salaire=" + salaire +
                '}';
    }
}
