package org.example.heritage;

public class Appartement extends House{


    public Appartement(Door door) {
        super(50, door);
    }

    @Override
    public String toString() {
        return "Appartement{" +
                "surface=" + surface +
                ", door=" + door +
                '}';
    }
}
