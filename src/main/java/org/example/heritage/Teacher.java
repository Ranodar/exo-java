package org.example.heritage;

public class Teacher extends Person {

    public Teacher(int age) {
        super(age);
    }

    public void explain(String subject){
        System.out.println("Explanation begins");
        System.out.println(subject);
    }
}
