package org.example.heritage;

public class Person {

    public int age;

    public Person(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int n) {
        age = n;
        this.age = age;
    }

    public void hello(){

        System.out.println("Hello");
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                '}';
    }
}
