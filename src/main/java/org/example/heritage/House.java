package org.example.heritage;

public class House {

    public int surface;
    public Door door;


    public House(int surface, Door door) {
        this.surface = surface;
        this.door = door;
    }

    public int getSurface() {
        return surface;
    }

    public void setSurface(int surface) {
        this.surface = surface;
    }

    public void display(){
        System.out.println("Je suis une maison, ma surface est de "+surface+" m²");
    }

    public void getDoor(){

    }

    @Override
    public String toString() {
        return "House{" +
                "surface=" + surface +
                '}';
    }
}
