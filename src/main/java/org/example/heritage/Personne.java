package org.example.heritage;

public class Personne {

    public String nom;
    public Appartement appartement;

    public Personne(String nom, Appartement appartement) {
        this.nom = nom;
        this.appartement = appartement;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Appartement getAppartement() {
        return appartement;
    }

    public void setAppartement(Appartement appartement) {
        this.appartement = appartement;
    }

    public void display(){
        System.out.println("Je m'appelle "+nom);
        appartement.display();
        appartement.door.display();
    }

    @Override
    public String toString() {
        return "Personne{" +
                "nom='" + nom + '\'' +
                ", appartement=" + appartement +
                '}';
    }
}
