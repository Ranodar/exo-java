package org.example.enums;

import java.util.Scanner;

public class ExoEnum {
    public static void getPriority(){
        //Priority priority = Priority.D;
        Scanner saisirEntree = new Scanner(System.in);
        System.out.println("Entrez le type de message :");
        String entree = saisirEntree.next();

        while (!(entree.equals("A")|| entree.equals("B")|| entree.equals("C")|| entree.equals("D"))){
            System.out.println(entree);
            System.out.println("Seuls les messages de type «A», «B», «C» ou «D» sont autorisés.");
            System.out.println("Entrez le type de message :");
            entree = saisirEntree.next();;
        }


        switch (entree){
            case "A":
                System.out.println(Priority.A.getMsgPriority());
                break;
            case "B":
                System.out.println(Priority.B.getMsgPriority());
                break;
            case "C":
                System.out.println(Priority.C.getMsgPriority());
                break;
            case "D":
                System.out.println(Priority.D.getMsgPriority());
                break;
        }
        System.out.println("Le message "+ Priority.valueOf(entree).getMsgPriority()+" du type "+entree);
    }
}
