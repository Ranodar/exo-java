package org.example.enums;

public enum Priority {
    A("HIGH"),
    B("MEDIUM"),
    C("LOW"),
    D("LOW");

    private String msgPriority;

    private Priority(String msg) { this.msgPriority = msg;}

    public String getMsgPriority() {return this.msgPriority;}
}
