package org.example.exo;

import com.sun.tools.jconsole.JConsoleContext;

import java.util.Scanner;

public class Switch {

    public static void getSwitchEvenement(){
        Scanner saisirEntree = new Scanner(System.in);
        System.out.println("Veuillez saisir votre commande :");
        String entree = saisirEntree.next();

        while (!entree.equals("ok")){
            switch (entree){
                case "login":
                    System.out.println("Please, enter your username");
                    break;
                case "sign_up":
                    System.out.println("Welcome!");
                    break;
                case "terminate_program":
                    System.out.println("Thank you! Good bye!");
                    break;
                case "main_menu":
                    System.out.println("Main menu");
                    break;
                case "about_app":
                    System.out.println("This app is created by me with support of me");
                    break;
                default:
                    System.out.println("Please enter one of these values : login, sign_up, terminate_program, main_menu, about_app");
                    break;
            }
            System.out.println("Veuillez saisir votre commande 2:");
            entree = saisirEntree.next();
        }
    }
}
