package org.example.exo;

import java.util.Arrays;
import java.util.Scanner;

public class Exo {
    public static void getComptageDeMot() {
        String phrase = "Phrase au pif pour";
        String[] mots = null;
        int count = 0;
        mots = phrase.split(" ");
        count = mots.length;
        System.out.println("Le nombre de mots est de "+ count);
    }
    public static void getComptageOccurrence(){
        String mot = "test";
        String l = "t";
        int count = 0;
        String[] lettres = mot.split("");
        for (int i = 0; i < lettres.length; i++) {
            if (lettres[i].equals(l)){
                ++count;
            }
        }
        System.out.println("Le nombre d'occurrences de la lettre "+l+" est de "+ count);


    }
    public static void getAnagramme(){
        String mot1 = "cafe";
        String mot2 = "efca";
        String[] tab1 = mot1.split("");
        String[] tab2 = mot2.split("");
        int count = 0;

        if (tab1.length == tab2.length){
            for(int i = 0; i < tab1.length-1; i++){
                int j = tab1.length-1-i;
                //System.out.println("lettre du mot 1 : "+ tab1[i]+" lettre du mot 2 : "+ tab2[j]);
                if(tab1[i].equals(tab2[j]))
                {
                    count = count+0;
                }else{
                    count = count+1;
                }
            }
            if(count == 0){
                System.out.println("Le mot "+mot2+" est un anagramme du mot "+mot1);
            }else {
                System.out.println("Le mot "+mot2+" n'est pas un anagramme du mot "+mot1);
            }
        }else {
            System.out.println("Le mot "+mot2+" est trop long ");
        }



    }
    public static void getPalindrome(){
        String mot1 = "kayak";
        String[] tab1 = mot1.split("");
        int count = 0;
        for (int i = 0; i < tab1.length/2; i++){
            if (tab1[i].equals(tab1[tab1.length-1-i])){
                count = count+0;
            }else{
                count = count+1;
            }
        }
        if(count == 0){
            System.out.println("Le mot "+mot1+" est un palindrome");
        }else {
            System.out.println("Le mot "+mot1+" n'est pas un palindrome");
        }
    }

    public static void getPyramide(){
        Scanner taillePyramide = new Scanner(System.in);
        System.out.println("Veuillez saisir la taille maximale de la pyramide :");
        int taille = taillePyramide.nextInt();

        for (int i = 0; i <= taille; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print("*");
            }
            System.out.println("");
        }
        for (int k = taille-1; k > 0; k--) {
            for (int l = 0; l < k; l++) {
                System.out.print("*");
            }
            System.out.println("");
        }
    }
}
