package org.example.exo;

import java.util.*;

public class Tableau {

    public static void getTailleTab(){
        int qty = 0;
        Scanner saisirEntree = new Scanner(System.in);
        System.out.println("Entrez la taille du tableau :");
        Integer entree = saisirEntree.nextInt();
        generateRandomArray(entree);
    }

    public static void generateRandomArray (int qty) {
        int[] tab = new int[qty];
        int[] tab2 = new int[tab.length*2];
        for (int i = 0; i < qty; i++) {
            Random number = new Random();
            int int_random = number.nextInt();
            tab[i] = int_random;
        }

        for(int i = 0; i < tab.length; i++){
            tab2[i] = tab[i];
        }
        int s = 0;
        for (int j = qty; j < (qty*2); j++) {
            tab2[j] = 2*tab[s];
            s = s+1;
        }
        System.out.println(Arrays.toString(tab));
        System.out.println(Arrays.toString(tab2));
    }
}
