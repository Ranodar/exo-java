package org.example.exo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Structure {

    public static void getTabExo1(){
        List<Integer> Liste = new ArrayList<Integer>();
        for (int i = 0; i < 20; i++) {
            Scanner saisirEntree = new Scanner(System.in);
            System.out.println("Veuillez saisir la note n°"+(i+1)+" : ");
            Integer entree = saisirEntree.nextInt();
            Liste.add(entree);
            System.out.println("la note "+Liste.get(i)+" a été enregistrée");
        }

        int moyenne= 0;
        for (int i = 0; i < 20; i++){
            //System.out.println(Liste.get(i));
            moyenne += Liste.get(i);
        }
        System.out.println("La moyenne de la classe est de "+(moyenne/20));
        
        Integer dernier = 21;
        Integer premier = 0;

        for (int i = 0; i < 20; i++) {
            if (Liste.get(i) >premier){
                premier = Liste.get(i);
            } else if (Liste.get(i) <dernier) {
                dernier = Liste.get(i);
            }
        }
        System.out.println("La note la plus basse est "+dernier+" et la note la plus élevée est "+premier);
    }

    public static void getDecalage(){
        String mot = "DECALAGE";
        //Scanner saisirEntree = new Scanner(System.in);
        //System.out.println("Veuillez saisir votre mot à décaler : ");
        //mot = saisirEntree.toString();
        String[] decalageTab = mot.split("");
        String save = null;

        save = decalageTab[0];
        for (int i = 0; i < decalageTab.length-1; i++) {
            decalageTab[i] = decalageTab[i+1];
            System.out.println(decalageTab[i]);
        }
        decalageTab[decalageTab.length-1] = save;
        System.out.println(decalageTab[decalageTab.length-1]);
    }

}
