package org.example.exo;

import java.util.Random;
import java.util.Scanner;

public class JustePrix {
    public static void trouverLePrix(){;
        int count = 5;
        Random rand = new Random();
        int numberAlea = rand.nextInt(50 - 0 + 1) + 0;
        Scanner saisirEntree = new Scanner(System.in);
        System.out.println("Trouvez le juste prix :");
        Integer entree = saisirEntree.nextInt();
        while (!(entree == numberAlea)) {
            if (count == 0){
                System.out.println("Vous avez épuisé vos chances, le juste prix était "+numberAlea+".");
                break;
            }
            if (entree > numberAlea){
                System.out.println("Le prix est plus petit.");
                System.out.println("Entrez un nombre :");
                entree = saisirEntree.nextInt();
            }else {
                System.out.println("Le prix est plus grand.");
                System.out.println("Entrez un nombre :");
                entree = saisirEntree.nextInt();
            }
            count = count-1;
            System.out.println("Il vous reste "+count+" coup(s) à jouer.");
        }
        if (entree == numberAlea){
            System.out.println("Bravo vous avez trouvé le juste prix "+numberAlea+" en "+count);
        }

    }
}
