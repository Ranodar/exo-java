package org.example.poo;

public class Payant extends Compte {

    protected int fraisOperation = 5;

    public Payant() {
        this.fraisOperation = fraisOperation;
    }

    public Payant(double solde) {
        super(solde);
        this.fraisOperation = fraisOperation;
    }

    public int getFraisOperation() {
        return fraisOperation;
    }

    public void retraitPayant(double number){
        number = number-(number*0.05);
        super.retrait(number);
        System.out.println("Votre nouveau solde est de "+solde);
    }

    public void versementPayant(double number){
        number = number-(number*0.05);
        super.versement(number);
        System.out.println("Votre nouveau solde est de "+solde);
    }

    @Override
    public String toString() {
        return "Payant{" +
                "fraisOperation= " + fraisOperation+"%"+
                ", id= " + id +
                ", solde= " + solde +
                '}';
    }
}
