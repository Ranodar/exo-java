package org.example.poo;

public class Simple extends Compte {

    double decouvert;

    public Simple(){

    }

    public Simple(double decouvert) {
        this.decouvert = decouvert;
    }

    public Simple(double solde, double decouvert) {
        super(solde);
        this.decouvert = decouvert;
    }

    public double getDecouvert() {
        return decouvert;
    }

    public void setDecouvert(double decouvert) {
        this.decouvert = decouvert;
    }

    public void verifDecouvert(double number){
        super.retrait(number);
        if ((decouvert+solde)<0){
            System.out.println("Vous dépassez le découvert autorisé de "+decouvert);
            System.out.println("Opération annulée");
            solde = solde + number;
        }else {
            System.out.println("Votre découvert restant est de "+(decouvert+solde));
        }

    }

    @Override
    public String toString() {
        return "Simple{" +
                "decouvert= " + decouvert +
                ", id= " + id +
                ", solde= " + solde +
                '}';
    }
}
