package org.example.poo;

public class Compte {

    protected int id;
    protected double solde;

    private static int nbCompte = 0;

    public Compte(){

    }

    public Compte(double solde) {
        this.id = ++nbCompte;
        this.solde = solde;
    }

    public int getId() {
        return id;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    public void versement(double number){
        this.solde = solde + number;
        System.out.println("Votre nouveau solde est de "+solde);
    }

    public void retrait(double number){
        this.solde = solde - number;
        System.out.println("Votre nouveau solde est de "+solde);
    }

    @Override
    public String toString() {
        return "Compte{" +
                "code= " + id +
                ", solde= " + solde +
                '}';
    }
}
