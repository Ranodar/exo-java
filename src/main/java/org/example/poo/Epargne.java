package org.example.poo;

public class Epargne extends Compte {

    protected double tauxInteret;

    public Epargne(){

    }

    public Epargne(double tauxInteret) {
        this.tauxInteret = tauxInteret;
    }

    public Epargne(double solde, double tauxInteret) {
        super(solde);
        this.tauxInteret = tauxInteret;
    }

    public double getTauxInteret() {
        return tauxInteret;
    }

    public void calculInteret(){
        solde = solde*(1+tauxInteret/100);
        System.out.println("Votre nouveau solde après calcul des intérêts est de "+solde);
    }

    @Override
    public String toString() {
        return "Epargne{" +
                "tauxInteret= " + tauxInteret +
                ", id= " + id +
                ", solde= " + solde +
                '}';
    }
}
