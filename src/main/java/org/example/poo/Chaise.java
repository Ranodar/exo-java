package org.example.poo;

public class Chaise {

    protected int nbPieds;
    protected String couleur;
    protected String matériaux;

    public Chaise(){

    }

    public Chaise(int nbPieds, String couleur, String matériaux) {
        this.nbPieds = nbPieds;
        this.couleur = couleur;
        this.matériaux = matériaux;
    }

    public int getNbPieds() {
        return nbPieds;
    }

    public void setNbPieds(int nbPieds) {
        this.nbPieds = nbPieds;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public String getMatériaux() {
        return matériaux;
    }

    public void setMatériaux(String matériaux) {
        this.matériaux = matériaux;
    }

    @Override
    public String toString() {
        return "Je suis une Chaise, avec "+nbPieds+" pieds en "+matériaux+" et de couleur "+couleur;
    }
}
